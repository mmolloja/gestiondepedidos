package com.example.milto.gestiondepedidos.dominio;

public class Administrador extends Usuario {

    public Administrador() {
    }

    public Administrador(Integer dni, String clave, String nombre) {
        super(dni, clave, nombre);
    }

    @Override
    public Boolean esAdministrador(){
        return true;
    }

    @Override
    public String toString() {
        return "Administrador{} " + super.toString() + "+ Rol: Administrador";
    }
}
