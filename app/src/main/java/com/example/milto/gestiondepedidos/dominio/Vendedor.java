package com.example.milto.gestiondepedidos.dominio;

public class Vendedor extends Usuario {

    public Vendedor() {
    }

    public Vendedor(Integer dni, String clave, String nombre) {
        super(dni, clave, nombre);
    }

    @Override
    public Boolean esVendedor(){
        return true;
    }

    @Override
    public String toString() {
        return "Vendedor{} " + super.toString() + " Rol : Vendedor";
    }
}
