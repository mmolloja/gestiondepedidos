package com.example.milto.gestiondepedidos.dominio;

public class Cliente {
    protected static Integer proximoId=0;
    private Integer idCliente;
    private String cuit;
    private String nombre;
    private String domicilio;

    public Cliente() {
    }

    public Cliente(String cuit, String nombre, String domicilio) {
        this.idCliente = proximoId;
        proximoId++;
        this.cuit = cuit;
        this.nombre = nombre;
        this.domicilio = domicilio;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    @Override
    public String toString() {
        return  "idCliente=" + this.idCliente +
                ", cuit='" + this.cuit + '\'' +
                ", nombre='" + this.nombre + '\'' +
                ", domicilio='" + this.domicilio + '\'' +
                '}';
    }
}
