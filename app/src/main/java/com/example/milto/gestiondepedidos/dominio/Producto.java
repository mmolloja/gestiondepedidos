package com.example.milto.gestiondepedidos.dominio;

public class Producto {
    protected static Integer proximoId=0;
    private Integer idProducto;
    private String nombre;
    private String tamanio;
    private double precioUnitario;
    private Boolean estado;
    private Integer stock;

    public Producto() {
    }

    public Producto(String nombre, String tamanio, double precioUnitario, Boolean estado, Integer stock) {
        this.idProducto = proximoId;
        proximoId++;
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.precioUnitario = precioUnitario;
        this.estado = estado;
        this.stock = stock;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "idProducto=" + idProducto +
                ", nombre='" + nombre + '\'' +
                ", tamanio='" + tamanio + '\'' +
                ", precioUnitario=" + precioUnitario +
                ", estado=" + estado +
                ", stock=" + stock +
                '}';
    }
}
