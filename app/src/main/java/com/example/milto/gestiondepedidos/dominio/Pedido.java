package com.example.milto.gestiondepedidos.dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pedido {
    protected static Integer proximoId=0;
    private Integer IdPedido;
    private Date FechaPedido;
    private Usuario usuario;
    private Cliente cliente;
    private List<Producto> detalle;

    public Pedido() {
    }

    public Pedido(Usuario usuario, Cliente cliente, List<Producto> detalle) {
        IdPedido = proximoId;
        proximoId++;
        FechaPedido = new Date();
        this.usuario = usuario;
        this.cliente = cliente;
        this.detalle = new ArrayList<>();;
    }

    public Integer getIdPedido() {
        return IdPedido;
    }

    public void setIdPedido(Integer idPedido) {
        IdPedido = idPedido;
    }

    public Date getFechaPedido() {
        return FechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        FechaPedido = fechaPedido;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Producto> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Producto> detalle) {
        this.detalle = detalle;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "IdPedido=" + IdPedido +
                ", FechaPedido=" + FechaPedido +
                ", usuario=" + usuario +
                ", cliente=" + cliente +
                ", detalle=" + detalle +
                '}';
    }
}
