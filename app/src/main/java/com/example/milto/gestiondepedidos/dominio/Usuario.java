package com.example.milto.gestiondepedidos.dominio;

public class Usuario {
    protected static Integer proximoId;
    private Integer idUsuario;
    private Integer dni;
    private String clave;
    private String nombre;

    public Usuario() {
    }

    public Usuario(Integer dni, String clave, String nombre) {
        this.idUsuario = proximoId;
        proximoId++;
        this.dni = dni;
        this.clave = clave;
        this.nombre = nombre;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "idUsuario=" + idUsuario +
                ", dni=" + dni +
                ", clave='" + clave + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public Boolean esAdministrador(){
        return false;
    }
    public Boolean esVendedor(){
        return false;
    }
}
